<?php

use App\Http\Controllers\DesignPattern\ChainController;
use App\Http\Controllers\DesignPattern\StateController;
use App\Http\Controllers\DesignPattern\FacadeController;
use App\Http\Controllers\DesignPattern\FactoryController;
use App\Http\Controllers\DesignPattern\ObserverController;
use App\Http\Controllers\DesignPattern\StrategyController;
use App\Http\Controllers\DesignPattern\TemplateController;
use App\Http\Controllers\DesignPattern\CompositeController;
use App\Http\Controllers\DesignPattern\SingletonController;
use App\Http\Controllers\DesignPattern\AggregationController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/singleton', [SingletonController::class, 'index']);

Route::get('/factory', [FactoryController::class, 'index']);

Route::get('/facade', [FacadeController::class, 'index']);

Route::get('/aggregation', [AggregationController::class, 'index']);

Route::get('/observer', [ObserverController::class, 'index']);

Route::get('/strategy', [StrategyController::class, 'index']);

Route::get('/template', [TemplateController::class, 'index']);

Route::get('/composite', [CompositeController::class, 'index']);

Route::get('/chain', [ChainController::class, 'index']);

Route::get('/state', [StateController::class, 'index']);

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<table>
    <tbody>
        <tr>Livrée</tr>
        <tr>
            <td>ConstructionHandler</td>
            <td>{{ $construction->handle('Livrée')}}</td>
        </tr>
        <tr>
            <td>AcheminementHandler</td>
            <td>{{ $acheminement->handle('Livrée')}}</td>
        </tr>
        <tr>
            <td>LivreeHandler</td>
            <td>{{ $livree->handle('Livrée')}}</td>
        </tr>
    </tbody>
</table>
<br>
<table>
    <tbody>
        <tr>Erreur Acheminement</tr>
        <tr>
            <td>ConstructionHandler</td>
            <td>{{ $construction->handle('Erreur Acheminement')}}</td>
        </tr>
        <tr>
            <td>AcheminementHandler</td>
            <td>{{ $acheminement->handle('Erreur Acheminement')}}</td>
        </tr>
        <tr>
            <td>LivreeHandler</td>
            <td>{{ $livree->handle('Erreur Acheminement')}}</td>
        </tr>
    </tbody>
</table>
<br>
<table>
    <tbody>
        <tr>Erreur Construction</tr>
        <tr>
            <td>ConstructionHandler</td>
            <td>{{ $construction->handle('Erreur Construction')}}</td>
        </tr>
        <tr>
            <td>AcheminementHandler</td>
            <td>{{ $acheminement->handle('Erreur Construction')}}</td>
        </tr>
        <tr>
            <td>LivreeHandler</td>
            <td>{{ $livree->handle('Erreur Construction')}}</td>
        </tr>
    </tbody>
</table>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <pre>
    Voiture : {{ $voiture->getPrixTotal() }}
        \_ Renault : {{ $renault->getPrixTotal() }}
            \_ Renault1 : {{ $renault1->getPrixTotal() }}
            \_ Renault2 : {{ $renault2->getPrixTotal() }}
            \_ Renault3 : {{ $renault3->getPrixTotal() }}
        \_ Opel : {{ $opel->getPrixTotal() }}
            \_ Opel1 : {{ $opel1->getPrixTotal() }}
            \_ Opel2 : {{ $opel2->getPrixTotal() }}
    </pre>
    
</body>
</html>
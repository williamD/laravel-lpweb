<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div>State: {{$construire->getState()}}</div>
    <div>{{$construire->demander()}}</div>
    <div>State: {{$construire->getState()}}</div>
    <div>{{$construire->construire()}}</div>
    <div>State: {{$construire->getState()}}</div>
    <div>{{$construire->peindre()}}</div>
    <div>State: {{$construire->getState()}}</div>
    <div>{{$construire->envoyer()}}</div>
</body>
</html>
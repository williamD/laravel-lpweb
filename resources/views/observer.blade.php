<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div>Sujet1: {{ $subject1->registerObserver($observer1) }}</div>
    <div>Sujet1: {{ $subject1->registerObserver($observer2) }}</div>
    <div>Sujet1: {{ $subject1->notifyObservers() }}</div>
    <div>Observer1: {{ $observer1->display() }}</div>
    <div>Observer2: {{ $observer2->display() }}</div>
    <div>Sujet1: {{ $subject1->unregisterObserver($observer1) }}</div>
    <div>Sujet1: {{ $subject1->unregisterObserver($observer2) }}</div>
    <div>Sujet2: {{ $subject2->registerObserver($observer1) }}</div>
    <div>Sujet2:{{ $subject2->registerObserver($observer2) }}</div>
    <div>Sujet2:{{ $subject2->notifyObservers() }}</div>
    <div>Observer1: {{ $observer1->display() }}</div>
    <div>Observer2: {{ $observer2->display() }}</div>
</body>
</html>
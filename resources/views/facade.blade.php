<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <label>Commande</label>
    <ul>
        @foreach ($commande as $element)
            <li>{{$element}}</li>
        @endforeach
    </ul>
    <div>
        {{ $usine->getFactory() }}
    </div>
    <div>
        {{ $concession->getConcession() }}
    </div>
    <div>
        {{ $facture->getFacture() }}
    </div>
</body>
</html>
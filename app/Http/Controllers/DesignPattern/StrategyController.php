<?php

namespace App\Http\Controllers\DesignPattern;

use Illuminate\Http\Request;
use App\Models\Strategy\Opel;
use App\Models\Strategy\Renault;
use App\Http\Controllers\Controller;
use App\Models\Strategy\FactureLine;
use App\Models\Strategy\StrategieOpel;
use App\Models\Strategy\StrategieRenault;

class StrategyController extends Controller
{
    public function index() {

        $voiture1 = new Opel();
        $voiture2 = new Renault();
        $factureLine1 = new FactureLine($voiture1);
        $factureLine2 = new FactureLine($voiture2);
        
        return view('strategy', [
            'factureLine1' => $factureLine1,
            'factureLine2' => $factureLine2,
        ]);
    }
}
<?php

namespace App\Http\Controllers\DesignPattern;

use Illuminate\Http\Request;
use App\Models\Facade\Facade;
use App\Http\Controllers\Controller;

class FacadeController extends Controller
{
    public function index() {
        $marques = ['Renault', 'Opel', 'Renault', 'Renault', 'Opel'];
        $commande = Facade::commander($marques, "Concession 1");

        return view('facade', [
            'commande' => $marques,
            'usine' => $commande[0],
            'concession' => $commande[1],
            'facture' => $commande[2],
        ]);
    }
}

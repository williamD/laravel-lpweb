<?php

namespace App\Http\Controllers\DesignPattern;

use App\Models\Facade\Concession;
use App\Http\Controllers\Controller;
use App\Models\Iteration\Aggregation;

class AggregationController extends Controller
{
    public function index() {
        $array = ['Opel', 'Renault', 'BMW', 'Audi', 'Dacia', 'Mercedes', 'Peugeot'];
        $concession = new Concession('aggregation');
        $concession->setArray($array);
        $aggregation = $concession->getIterator();

        return view('aggregation', [
            'aggregation' => $aggregation,
        ]);
    }
}

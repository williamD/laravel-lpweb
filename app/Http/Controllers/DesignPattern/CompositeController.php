<?php

namespace App\Http\Controllers\DesignPattern;

use Illuminate\Http\Request;
use App\Models\Composite\Leaf;
use App\Models\Composite\Composite;
use App\Http\Controllers\Controller;

class CompositeController extends Controller
{
    public function index() {
        $renault1 = new Leaf(9900);
        $renault2 = new Leaf(18500);
        $renault3 = new Leaf(12300);
        $opel1 = new Leaf(17600);
        $opel2 = new Leaf(14200);
        $renault = new Composite();
        $opel = new Composite();
        $voiture = new Composite();

        $renault->add($renault1);
        $renault->add($renault2);
        $renault->add($renault3);
        $opel->add($opel1);
        $opel->add($opel2);
        $voiture->add($renault);
        $voiture->add($opel);

        return view('composite', [
            'renault1' => $renault1,
            'renault2' => $renault2,
            'renault3' => $renault3,
            'opel1' => $opel1,
            'opel2' => $opel2,
            'renault' => $renault,
            'opel' => $opel,
            'voiture' => $voiture,
        ]);
    }
}

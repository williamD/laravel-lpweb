<?php

namespace App\Http\Controllers\DesignPattern;

use App\Models\Singleton\Singleton;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SingletonController extends Controller
{
    public function index() {
        $a = Singleton::getInstance();
        $b = Singleton::getInstance();
        
        return view('singleton', [
            'a' => $a,
            'b' => $b,
        ]);
    }
}

<?php

namespace App\Http\Controllers\DesignPattern;

use Illuminate\Http\Request;
use App\Models\Observer\Subject;
use App\Models\Observer\Observer;
use App\Http\Controllers\Controller;

class ObserverController extends Controller
{
    public function index() {
        $subject1 = new Subject();
        $subject2 = new Subject();
        $o1 = new Observer();
        $o2 = new Observer();
        
        return view('observer', [
            'subject1' => $subject1,
            'subject2' => $subject2,
            'observer1' => $o1,
            'observer2' => $o2,
        ]);
    }
}

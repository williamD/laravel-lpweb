<?php

namespace App\Http\Controllers\DesignPattern;

use Illuminate\Http\Request;
use App\Models\State\Construire;
use App\Models\State\StateDemande;
use App\Http\Controllers\Controller;

class StateController extends Controller
{
    public function index() {
        $construire = new Construire(new StateDemande());
        return view('state', [
            'construire' => $construire,
        ]);
    }
}

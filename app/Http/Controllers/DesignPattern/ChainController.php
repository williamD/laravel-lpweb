<?php

namespace App\Http\Controllers\DesignPattern;

use Illuminate\Http\Request;
use App\Models\Chain\LivreeHandler;
use App\Http\Controllers\Controller;
use App\Models\Chain\AcheminementHandler;
use App\Models\Chain\ConstructionHandler;

class ChainController extends Controller
{
    public function index() {
        $construction = new ConstructionHandler();
        $acheminement = new AcheminementHandler();
        $livree = new LivreeHandler();

        $construction->setNext($acheminement)->setNext($livree);

        return view('chain', [
            'construction' => $construction,
            'acheminement' => $acheminement,
            'livree' => $livree,
        ]);
    }
}

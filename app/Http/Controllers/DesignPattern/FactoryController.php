<?php

namespace App\Http\Controllers\DesignPattern;

use Illuminate\Http\Request;
use App\Models\Factory\VoitureFactory;
use App\Http\Controllers\Controller;

class FactoryController extends Controller
{
    public function index() {
        $usine = new VoitureFactory();
        $opel = $usine->creerVoiture("Opel");
        $renaud = $usine->creerVoiture("Renault");
        return view('factory', [
            'opel' => $opel,
            'renault' => $renaud,
        ]);
    }
}

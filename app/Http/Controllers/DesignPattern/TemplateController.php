<?php

namespace App\Http\Controllers\DesignPattern;

use Illuminate\Http\Request;
use App\Models\Template\SubClass1;
use App\Models\Template\SubClass2;
use App\Http\Controllers\Controller;

class TemplateController extends Controller
{
    public function index() {
        $class1 = new SubClass1;
        $class2 = new SubClass2;
        
        return view('template', [
            'class1' => $class1,
            'class2' => $class2,
        ]);
    }
}

<?php

namespace App\Models\Factory;

use App\Models\Factory\Opel;
use App\Models\Factory\Renault;

class VoitureFactory
{

    public function creerVoiture($marque) {
        if($marque == 'Opel') {
            return new Opel();
        } elseif($marque == 'Renault') {
            return new Renault();
        } else {
            throw new \Exception("Marque $marque inconnue");
        }
    }

    public function getFactory() {
        return get_class();
    }

}
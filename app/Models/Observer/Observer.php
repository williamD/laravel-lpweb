<?php

namespace App\Models\Observer;

class Observer
{

    private $notifications = 0;

    public function update() {
        echo "Observer: update\n";
        $this->notifications++;
    }

    public function display() {
        return $this->notifications." notification(s) reçue(s)\n";
    }

}
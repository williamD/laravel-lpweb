<?php

namespace App\Models\Observer;

use App\Models\Observer\Observer;

class Subject
{

    private $observers = [];

    public function __construct() {
        $this->observers = new \SplObjectStorage();
    }

    public function registerObserver(Observer $observer) {
        echo "registerObserver\n";
        $this->observers->attach($observer);
    }

    public function unregisterObserver(Observer $observer) {
        echo "unregisterObserver\n";
        $this->observers->detach($observer);
    }

    public function notifyObservers() {
        echo "notifyObservers\n";
        foreach($this->observers as $observer) {
            $observer->update();
        }
    }

}
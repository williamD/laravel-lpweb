<?php

namespace App\Models\Template;

abstract class Template
{

    public function getText() {
        $this->getMarque()->getOptions();
    }

    abstract public function getMarque();
    abstract public function getOptions();

}
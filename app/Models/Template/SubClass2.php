<?php

namespace App\Models\Template;

use App\Models\Template\Template;

class SubClass2 extends Template
{

    public function getMarque() {
        return $this;
    }

    public function getOptions() {
        echo "sub class 2";
    }

}
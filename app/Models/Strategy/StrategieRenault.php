<?php

namespace App\Models\Strategy;

use App\Models\Strategy\Voiture;
use App\Models\Strategy\Strategie;

class StrategieRenault implements Strategie
{
    public function tva(): string {
        return "TVA 10%";
    }
}
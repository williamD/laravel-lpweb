<?php

namespace App\Models\Strategy;

use App\Models\Strategy\Voiture;

interface Strategie
{
    public function tva(): string;
}
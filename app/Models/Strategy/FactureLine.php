<?php

namespace App\Models\Strategy;

use App\Models\Strategy\Voiture;
use App\Models\Strategy\Strategie;
use App\Models\Strategy\StrategieRenault;

class FactureLine
{

    private $strategie;
 
    public function __construct(Voiture $voiture) {
        if(get_class($voiture) == 'App\Models\Strategy\Renault') {
            $this->strategie = new StrategieRenault();
        } else {
            $this->strategie = new StrategieOpel();
        }
    }

    public function getFactureLine() {
        return $this->strategie->tva();
    }

}
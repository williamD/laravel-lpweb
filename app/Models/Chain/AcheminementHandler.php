<?php

namespace App\Models\Chain;

use App\Models\Chain\Handler;

class AcheminementHandler extends Handler
{

    public function handle(string $request): ?string {
        if ($request === "Erreur Acheminement") {
            return get_class()." | Erreur Acheminement traitée\n";
        } else {
            return parent::handle($request);
        }
    }

}
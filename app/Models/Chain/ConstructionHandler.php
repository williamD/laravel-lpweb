<?php

namespace App\Models\Chain;

use App\Models\Chain\Handler;

class ConstructionHandler extends Handler
{

    public function handle(string $request): ?string {
        if ($request === "Erreur Construction") {
            return get_class()." | Erreur Construction traitée\n";
        } else {
            return parent::handle($request);
        }
    }

}
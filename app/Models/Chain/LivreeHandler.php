<?php

namespace App\Classes\Chain;

use App\Classes\Chain\Handler;

class LivreeHandler extends Handler
{

    public function handle(string $request): ?string {
        if ($request === "Livrée") {
            return get_class()." | Livraison traitée\n";
        } else {
            return parent::handle($request);
        }
    }

}
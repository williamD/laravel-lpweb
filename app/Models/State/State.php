<?php

namespace App\Models\State;

abstract class State
{
    abstract public function action();
}
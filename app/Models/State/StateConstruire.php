<?php

namespace App\Models\State;

use App\Models\State\State;
use App\Models\State\StatePeindre;

class StateConstruire extends State
{

    public function action() {
        echo "-> peindre\n";
        return new StatePeindre();
    }

}
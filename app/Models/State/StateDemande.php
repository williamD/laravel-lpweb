<?php

namespace App\Models\State;

use App\Models\State\State;
use App\Models\State\StateConstruire;

class StateDemande extends State
{

    public function action() {
        echo "-> construire\n";
        return new StateConstruire();
    }

}
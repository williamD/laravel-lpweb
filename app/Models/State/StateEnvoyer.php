<?php

namespace App\Models\State;

use App\Models\State\State;
use App\Models\State\StateDemande;

class StateEnvoyer extends State
{

    public function action() {
        echo "-> demander\n";
        return new StateDemande();
    }

}
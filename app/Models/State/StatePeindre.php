<?php

namespace App\Models\State;

use App\Models\State\State;
use App\Models\State\StateEnvoyer;

class StatePeindre extends State
{

    public function action() {
        echo "-> envoyer\n";
        return new StateEnvoyer();
    }

}
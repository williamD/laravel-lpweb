<?php

namespace App\Models\State;

use App\Models\State\State;
use App\Models\State\StateDemande;

class Construire
{

    private $state;

    public function __construct(State $state) {
        $this->setState($state);
    }

    public function setState(State $state) {
        $this->state = $state;
    }

    public function getState() {
        return get_class($this->state);
    }

    public function construire() {
        $this->setState($this->state->action());
    }

    public function demander() {
        $this->setState($this->state->action());
    }

    public function envoyer() {
        $this->setState($this->state->action());
    }

    public function peindre() {
        $this->setState($this->state->action());
    }

}
<?php

namespace App\Models\Composite;

abstract class Component
{

   abstract public function getPrixTotal();

}
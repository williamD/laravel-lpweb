<?php

namespace App\Models\Composite;

use App\Models\Composite\Component;

class Leaf extends Component
{

    private $prix = 10000;

    public function __construct(int $prix) {
        $this->prix = $prix;
    }

    public function getPrixTotal(): int {
        return $this->prix;
    }

}
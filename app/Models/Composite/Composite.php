<?php

namespace App\Models\Composite;

use App\Models\Composite\Component;

class Composite extends Component {

    protected $children;

    public function __construct() {
        $this->children = new \SplObjectStorage();
    }

    public function add(Component $component): void {
        $this->children->attach($component);
    }

    public function getPrixTotal(): int {
        $total = 0;
        foreach($this->children as $child) {
            $total += $child->getPrixTotal();
        }
        return $total;
    }

}
<?php

namespace App\Models\Singleton;

class Singleton
{

    private static $count = 0;
    private static $instance = null;
    
    private function __construct() {}

    public function increment() {
        self::$count++;
        return self::$count;
    }

    public static function getInstance(): Singleton {
        if( static::$instance == null){
            static::$instance = new static();
        }
        return static::$instance;
    }
}
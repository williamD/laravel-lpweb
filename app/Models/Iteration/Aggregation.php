<?php

namespace App\Models\Iteration;

class Aggregation
{

    private $count = 0;
    private $array = [];

    public function __construct($array) {
        $this->array = $array;
    }

    public function hasNext() {
        return isset($this->array[$this->count + 1]);
    }

    public function next() {
        if($this->hasNext()) {
            $this->count++;
            return $this->array[$this->count];
        } else {
            throw new \OutOfBoundException();
        }
    }

}
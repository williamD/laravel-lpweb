<?php

namespace App\Models\Facade;

class Facture
{

    private $voitures = [];

    public function __construct($voitures) {
        foreach($voitures as $voiture) {
            $this->voitures[] = $voiture;
        }
    }

    public function getFacture() {
        return "Facture de ".count($this->voitures)." voitures";
    }
}
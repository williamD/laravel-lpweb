<?php

namespace App\Models\Facade;

use App\Models\Iteration\Aggregation;

class Concession
{

    private $name;
    private $voitures = [];

    public function __construct($name) {
        $this->name = $name;
    }

    public function recupererVoiture($voitures) {
        foreach($voitures as $voiture) {
            $this->voitures[] = $voiture;
        }
    }

    public function getConcession() {
        return "Concession ".$this->name." contenant ".count($this->voitures)." voitures";
    }

    public function setArray($array) {
        $this->voitures = $array;
    }

    public function getIterator() {
        return new Aggregation($this->voitures);
    }

}
<?php

namespace App\Models\Facade;

use App\Models\Facade\Concession;
use App\Models\Factory\VoitureFactory;

class Facade
{

    public static function commander($marques, $nomConcession) {
        $usine = new VoitureFactory();
        $voitures = [];
        foreach($marques as $marque) {
            $voiture = $usine->creerVoiture($marque);
            $voitures[] = $voiture;
        }
        $concession = new Concession($nomConcession);
        $concession->recupererVoiture($voitures);
        $facture = new Facture($voitures);

        return [$usine, $concession, $facture];
    }

}